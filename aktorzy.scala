import akka.actor.{ActorSystem, Actor, ActorLogging, ActorRef, Props}
import javax.sound.midi.Receiver


case class Oblicz(n: Int)
case class Wynik(n: Int, fib: BigInt)

class Boss extends Actor with ActorLogging {
  def receive: Actor.Receive = {
    case Oblicz(n) =>
        val nadzorca = context.actorOf(Props(Nadzorca()), s"nadzorca")
        nadzorca ! Oblicz(n)
    case Wynik(n, fib) =>
      log.info(s"$n wyraz ciągu to $fib")
  }
}


class Nadzorca(cache: Map[Int, BigInt] = Map(1 -> 1, 2 -> 1), doZrobienia: Set[Int] = Set())extends Actor with ActorLogging {
  def receive: Actor.Receive = {
    case Oblicz(n) =>
      if cache.contains(n) then
        sender() ! Wynik(n, cache(n))
      else
        val pracownik = context.actorOf(Props(Pracownik(n)), s"pracownik$n")
        pracownik ! Oblicz(n)
        context.become(obliczanie(List((n,sender())), cache))
      
}
  def obliczanie(przekaz_dalej: List[(Int,ActorRef)], cache: Map[Int, BigInt]):Receive ={
      case Oblicz(n)=>{
        if cache.contains(n) then
        sender() ! Wynik(n, cache(n))
        else
          val pracownik = context.actorOf(Props(Pracownik(n)), s"pracownik$n")
          pracownik ! Oblicz(n)
          context.become(obliczanie((n,sender())::przekaz_dalej, cache))
      }
      case Wynik(n, fib) =>{
        //dla kazdego w przekaz_dalej  odsylamy mu wynik n,fib. Mozna za pomocą fora
        
      }
  }
}

class Pracownik(k: Int) extends Actor with ActorLogging{
  def receive: Receive = {
      case  Oblicz(k) =>{
        sender() ! Oblicz(k-1)
        sender() ! Oblicz(k-2)
      }
      case Wynik(n,fib) =>{
        //przechowac w tozsamosci, wynik, bo jeden przyjdzie wczesniej niz drugi. jezeli beda obie to zmieniamy tozsamosc odsylamy
      }

  }
  //w nowym kontekście sumujemy te dwa wyniki
}

@main def main: Unit = {
  val system = ActorSystem("Fibonacci")
    val Boss = system.actorOf(Props[Boss](), "boss")
    Boss ! Oblicz(2)
}
