def quickSort(A, p, r):
    if p < r:
        q = partition(A, p, r)
        quickSort(A, p, q)
        quickSort(A, q+1, r)


def partition(A, p, r):
    x = A[r]
    i = p-1
    for j in range(p, r+1):
        if A[j] <= x:
            i = i+1
            A[i], A[j] = A[j], A[i]
    if i < r:
        return i
    else:
        return i-1


A = []
quickSort(A, 0, len(A)-1)
