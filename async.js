const axios = require("axios");

const getRndInteger = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

const tab = [getRndInteger(0,99),getRndInteger(0,99),getRndInteger(0,99),getRndInteger(0,99),getRndInteger(0,99)];

//funkcja do tworzenia 1 obiektu

const poId = (id) => {
    return axios.get("https://jsonplaceholder.typicode.com/posts/" + id)
        .then(//tutaj idą dane bez komentarzy
            (suroweDane) => {
                const poprawneDane = suroweDane.data;
                const {
                    id: zmiennaID,
                    userId: zmiennaUserId,
                    ...reszta
                } = poprawneDane
                const obiektWynikowy = {
                    "entry": reszta
                }
                //    console.log(obiektWynikowy);
                return obiektWynikowy;
            }
        )//obsługa komentarzy
        .then(
            (obiektWynikowy) =>
                axios.get("https://jsonplaceholder.typicode.com/posts/" + id + "/comments")
                    .then((suroweKomentarze) => {
                        const obiektZkomentarzami = suroweKomentarze.data.reduce((acc, curr) => {
                            const {
                                id: idKomentarze,
                                postId: idPost,
                                ...resztaKomentarze
                            } = curr
                            acc.push(resztaKomentarze)
                            return acc;
                        }, []);
                        obiektWynikowy["comments"] = obiektZkomentarzami;
                        return obiektWynikowy;
                    })      
        )
}

const tablica_wynikowa = [];
tab.forEach(element =>{ 
    tablica_wynikowa.push(poId(element));
})

Promise.all(tablica_wynikowa).then((results) => {
    console.log(results);
  });
