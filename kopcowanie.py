# def heapify_rekurencyjnie(A, heapSize, i):
#     l = 2*i+1
#     r = 2*i+2
#     if l < heapSize and A[l] > A[i]:
#         largest = l
#     else:
#         largest = i
#     if r < heapSize and A[r] > A[largest]:
#         largest = r
#     if largest != i:
#         A[i], A[largest] = A[largest], A[i]
#         heapify_rekurencyjnie(A, heapSize, largest)
#     return A


def heapify_iteracyjnie(A, heapSize, i):
    while True:
        l = 2*i+1
        r = 2*i+2
        largest = i
        if l < heapSize and A[l] > A[largest]:
            largest = l
        if r < heapSize and A[r] > A[largest]:
            largest = r
        if largest != i:
            A[i], A[largest] = A[largest], A[i]
            i = largest
        else:
            break
    return A


def buildHeap(A):
    heapSize = len(A)
    k = int((len(A)-2)/2)
    for i in range(k, -1, -1):
        heapify_iteracyjnie(A, heapSize, i)

    return A


def heapSort(A):
    A = buildHeap(A)
    heapSize = len(A)
    for i in range(len(A)-1, 0, -1):
        A[0], A[heapSize-1] = A[heapSize-1], A[0]
        heapSize -= 1
        heapify_iteracyjnie(A, heapSize, 0)
    return A


#wczytywanie z pliku

A = []
with open('dane.txt') as odczyt:
    linie = odczyt.readlines()

for linia in linie:
    A.append(int(linia.strip()))


#zapis do pliku
B = heapSort(A)

zapis = open("wynik.txt", "w")
for i in range(len(B)):
    zapis.write(str(B[i]))
    zapis.write('\n')
zapis.close()
