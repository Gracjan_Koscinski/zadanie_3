import sys
import random
from timeit import default_timer as timer
sys.setrecursionlimit(1000000)


def quickSort(A, p, r):
    if p < r:
        q = partition(A, p, r)
        quickSort(A, p, q)
        quickSort(A, q + 1, r)
    else:
        sortowanie_babelkowe(A, p, r)


def quickSort_babelkowo(A, p, r):
    if r - p + 1 > 7:
        q = partition(A, p, r)
        quickSort_babelkowo(A, p, q)
        quickSort_babelkowo(A, q + 1, r)
    else:
        sortowanie_babelkowe(A, p, r)


def partition(A, p, r):
    x = A[r]
    i = p - 1
    for j in range(p, r + 1):
        if A[j] <= x:
            i = i + 1
            A[i], A[j] = A[j], A[i]
    if i < r:
        return i
    else:
        return i - 1


def sortowanie_babelkowe(A, p, r):
    for i in range(p, r + 1):
        for j in range(p, r + 1):
            if A[i] < A[j]:
                A[i], A[j] = A[j], A[i]
    return A


# A = [10, 3, 4, 5, 6, 8, 9, 1]
# quickSort(A, 0, len(A) - 1)
# B = [65, 3, 1, 2, 3, 4, 5, 10, 32, 11]
# print(sortowanie_babelkowe(B, 0, len(B) - 1))
lista = []
for _ in range(4000):
    lista.append(random.randint(0, 1000))
nn = [lista]
for n in nn:
    start = timer()
    quickSort(n, 0, len(n) - 1)
    stop = timer()
    Tn = stop - start
    print(n, Tn)

# for n in nn:
#     start = timer()
#     quickSort_babelkowo(n, 0, len(n) - 1)
#     stop = timer()
#     Tn = stop - start
#     print(n, Tn)
#
# for n in nn:
#     start = timer()
#     sortowanie_babelkowe(n, 0, len(n) - 1)
#     stop = timer()
#     Tn = stop - start
#     print(n, Tn)
